function [faces_full,vertices_full,Bc,nodeBN_v,nodeBN_f,indv] = TwoBubbleGenerator_reflection(sq,a0)

[faces,vertices] = SphereMeshZ(sq,1);
%x=0 - reflection plane
vertices=a0*vertices;
%h=0.7;%0.7;%0.7;
%h=1.2;
 h = 3;
 d=(1+h/2)*a0;
% d = 5*a0;
vertices1=vertices;
vertices2=vertices;
vertices1(:,1)=vertices(:,1)-d;
vertices2(:,1)=-vertices1(:,1);

nodeBN_v=length(vertices);
nodeBN_f=length(faces);

vertices_full=zeros(nodeBN_v*2,3);
faces_full=zeros(nodeBN_f*2,3);
indv=zeros(nodeBN_v*2,1);

vertices_full(1:nodeBN_v,:)=vertices1;
faces_full(1:nodeBN_f,:)=faces;
indv(1:nodeBN_v,:)=1;

vertices_full(nodeBN_v+1:end,:)=vertices2;
% vertices_full = vertices_full + d;

faces_full(nodeBN_f+1:end,:)=[faces(:,2) faces(:,1) faces(:,3)]+nodeBN_v;
indv(nodeBN_v+1:end)=2;

Bc=zeros(2,3);
Bc(1,:)=[-d 0 0];
Bc(2,:)=[d 0 0];
% Bc(1,:)=[0 0 0];
% Bc(2,:)=[d 0 0];