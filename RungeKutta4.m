function [vnew, drb, phinew, dphib, Rpnew, dRp, Upnew, dUp, DTpnew, dDTp, Mpnew, dMp] = RungeKutta4(time, rb, phib, Rp, Up, DTp, Mp)
% function [rb_new, drb_k1, phib_new, dphib_k1, Rp_new, dRp_k1, Up_new, dUp_k1] = RungeKutta4(time, rb, phib, Rp, Up)
%one step of the Runge-Kutta 4th order with saving of the rhs at t=time
global ht
% h = ht;
% %============= my test zone =========================
% [drb_k1, dphib_k1, dRp_k1, dUp_k1] = RHS(time, rb, phib, Rp, Up);
% [drb_k2, dphib_k2, dRp_k2, dUp_k2] = RHS(time+h/2, rb + h/2*drb_k1, phib + h/2*dphib_k1, Rp + h/2*dRp_k1, Up + h/2*dUp_k1);
% [drb_k3, dphib_k3, dRp_k3, dUp_k3] = RHS(time+h/2, rb + h/2*drb_k2, phib + h/2*dphib_k2, Rp + h/2*dRp_k2, Up + h/2*dUp_k2);
% [drb_k4, dphib_k4, dRp_k4, dUp_k4] = RHS(time+h,   rb + h*drb_k3,   phib + h*dphib_k3,   Rp + h*dRp_k3,   Up + h*dUp_k3);
% 
% rb_new   = rb   + h/6*(drb_k1   + 2*drb_k2   + 2*drb_k3   + drb_k4); % get new rb
% phib_new = phib + h/6*(dphib_k1 + 2*dphib_k2 + 2*dphib_k3 + dphib_k4);
% Rp_new   = Rp   + h/6*(dRp_k1   + 2*dRp_k2   + 2*dRp_k3   + dRp_k4);
% Up_new   = Up   + h/6*(dUp_k1   + 2*dUp_k2   + 2*dUp_k3   + dUp_k4);
% %====================================================



[drb, dphib, dRp, dUp, dDTp, dMp] = RHS(time, rb, phib, Rp, Up, DTp, Mp);

vnew   = rb   + .5*ht*drb;
phinew = phib + .5*ht*dphib;
Rpnew  = Rp   + .5*ht*dRp;
Upnew  = Up   + .5*ht*dUp;
DTpnew = DTp  + .5*ht*dDTp;
Mpnew = Mp   + .5*ht*dMp;
timenew = time + .5*ht;
[rk2,rk2_,rk2p,rk2p_, dDTp_k2, dMp_k2] = RHS(timenew, vnew, phinew, Rpnew, Upnew, DTpnew, Mpnew);


vnew   = vnew+0.5*ht*(rk2-drb);
phinew = phinew+0.5*ht*(rk2_-dphib);
Rpnew  = Rpnew+0.5*ht*(rk2p-dRp);
Upnew  = Upnew+0.5*ht*(rk2p_-dUp);
DTpnew = DTpnew+0.5*ht*(dDTp_k2-dDTp);
Mpnew  = Mpnew+0.5*ht*(dMp_k2-dMp);
[rk3,rk3_,rk3p,rk3p_, dDTp_k3, dMp_k3] = RHS(timenew,vnew,phinew,Rpnew,Upnew, DTpnew, Mpnew);

vnew=vnew+ht*(rk3-0.5*rk2);
phinew=phinew+ht*(rk3_-0.5*rk2_);
Rpnew=Rpnew+ht*(rk3p-0.5*rk2p);
Upnew=Upnew+ht*(rk3p_-0.5*rk2p_);
DTpnew = DTpnew +ht*(dDTp_k3-0.5*dDTp_k2);
Mpnew=Mpnew+ht*(dMp_k3-0.5*dMp_k2);

rk3=rk2-2*rk3;
rk3_=rk2_-2*rk3_;
rk3p=rk2p-2*rk3p;
rk3p_=rk2p_-2*rk3p_;
dDTp_k3=dDTp_k2-2*dDTp_k3;
dMp_k3=dMp_k2-2*dMp_k3;
timenew=timenew+.5*ht;

[rk2,rk2_,rk2p,rk2p_, dDTp_k2, dMp_k2] = RHS(timenew,vnew,phinew,Rpnew,Upnew, DTpnew, Mpnew);
vnew    = vnew+(ht/6)*(rk2+2*rk3+drb);
phinew  = phinew+(ht/6)*(rk2_+2*rk3_+dphib);
Rpnew   = Rpnew+(ht/6)*(rk2p+2*rk3p+dRp);
Upnew   = Upnew+(ht/6)*(rk2p_+2*rk3p_+dUp);
DTpnew  = DTpnew+(ht/6)*(dDTp_k2+2*dDTp_k3+dDTp);
Mpnew   = Mpnew+(ht/6)*(dMp_k2+2*dMp_k3+dMp);

% max(rb_new   - vnew)
% max(phib_new - phinew)
% max(Rp_new   - Rpnew)
% max(Up_new   - Upnew)
%  error(1)