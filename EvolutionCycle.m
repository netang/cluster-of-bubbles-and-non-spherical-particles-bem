
handleGraphic = openfig('emptytime.fig');
    
h = 1;%0
it = 1;
time = 0; 


Vg_mas = zeros(nt-1, NB);
% Rp_mas = zeros(nt-1, 3);
% Up_mas = zeros(nt-1, 3);
Bc_mas = zeros(nt-1, NB, 3);
time_mas = zeros(nt-1, 1);

Visualize;


isave=1;

if multistep_mem > 1
    sizev = size(rb); 
    lszv = length(sizev); 
    sizev(lszv+1) = multistep_mem-1;
    vmem = zeros(sizev);
    phimem = zeros(length(rb),multistep_mem-1);
    Rpmem = zeros(NP, 3, multistep_mem-1);
    Upmem = zeros(NP, 3, multistep_mem-1);
end;
           

tcpu0 = cputime;
it0 = it;
% debug_i = 1; % delete
% main time loop
% why while loop? i can do it by use loop for
while it < nt    %nt - количество временных отрезков = (time/step); it - текущий номер шага
   
    tcpu = cputime;
   
    if multistep_mem > 1
        if it < multistep_mem % типо для начала просчитываем методом Рунге-Кутта, чтобы получить несколько значений производных для метода Адамса
            
            [rb, drb, phib, dphib, Rp, dRp, Up, dUp, DTp, dDTp, Mp, dMp] = RungeKutta4(time, rb, phib, Rp, Up, DTp, Mp);
            mempos=multistep_mem-it;
            vmem(:,:,mempos) = drb;
            phimem(:,mempos) = dphib;
            size(Rpmem)
            size(dRp)
            Rpmem(:, :, mempos) = dRp;
            Upmem(:, :, mempos) = dUp;
            %%DTpmem() = DTp;
            %%Mpmem()  = Mp;

        else
            if type_scheme == 1 % adamsa bashforta
%                 disp('считает Адамсом-Башфортом');
                          [rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem] = ...
                ab_n(time, rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem);  
            else
                
                           [rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem] = ...
                 abm6(time, rb, vmem, phib, phimem, Rp, Rpmem, Up, Upmem);
%                 disp('считает Адамсом-Башфортом-Молтоном');
            end;
            
        end;
    else
        
         [rb, drb, phib, dphib, Rp, dRp, Up, dUp, DTp, dDTp, Mp, dMp] = ...
        RungeKutta4(time, rb, phib, Rp, Up, DTp, Mp);

    end;
    
    Visualize;
        
    it=it+1; 
    time=(it-1)*ht;
     
    %----------------------------------------------------------------------
    %-------------------------просто вывод времени-------------------------
    steptime = cputime-tcpu;
    passedtime = cputime-tcpu0;
    lefttime = passedtime*(nt-it)/(it-it0);
    step = time/ht0;
    fprintf('step = %f, time = %f,  cputime step = %f, cputime passed = %f, cputime left = %f\n',...
             step,      time,       steptime,          passedtime,          lefttime);        
    %----------------------------------------------------------------------
    
end;