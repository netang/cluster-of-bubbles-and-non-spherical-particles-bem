function [drb, dphib, dRp, dUp, dDTp, dMp] = RHS(time, rb, phib, Rp, Up, DTp, Mp) % Mp already exist
% right-hand side computation
% input:   rb,        phib,        Rp,       Up         DTp        Mp         (+ time)
% output:  d(rb)/dt   d(phib)/dt   d(Rp)/dt  d(Up)/dt   d(DTp)/dt  d(Mp)/dt


size_rb = size(rb)
size_phib = size(phib)
sizeRp = size(Rp)
sizeUp = size(Up)
sizeDTp = size(DTp)
sizeMp = size(Mp)
% error(1)

%================================================================
%===========  Global variables for this function ================
global facesb facesp
% facesb - all bubbles faces
% facesp - all particles faces
% faces  - all faces

% global parameters: g, rho_l, rho_p[], kappa, sigma, k
global sigma rho_l g alpha m_p rho_p
global Pg0 kappa omega Ap P0 Filter

global rp_local_vert Nvert
% Nvert - Number of vertices in one mesh

global Vg Vg0

global NB NP
% NB - number of bubbles
% NP - number of particles
% NBV - number of vertices all bubbles

global local_Ip
% local_Ip is constant tensor determined by the particle shape;

global a b c
%=================================================================


for i = 1:NB
    k = (Nvert*(i-1)+1:Nvert*i)';
    phib(k) = Filter*phib(k);
    rb(k,1) = Filter*rb(k,1);
    rb(k,2) = Filter*rb(k,2);
    rb(k,3) = Filter*rb(k,3);
end;

%rp = rp_local_vert + kron(Rp, ones( Nvert, 1) ); % determine current mesh for particles with help of Rp;
rp = zeros(NP*Nvert, 3);
for i = 1:NP
    size(rp((i-1)*Nvert+1:i*Nvert, :))
    size(DTp((i-1)*3+1:i*3,:))
    size(  (DTp((i-1)*3+1:i*3,:)*rp_local_vert((i-1)*Nvert+1:i*Nvert, :)')'   )
    size(kron(Rp(i,:), ones( Nvert, 1) ))
    rp((i-1)*Nvert+1:i*Nvert, :) = (DTp((i-1)*3+1:i*3,:)*rp_local_vert((i-1)*Nvert+1:i*Nvert, :)')' + kron(Rp(i,:), ones( Nvert, 1) );
end

[normalsb, areasb] = get_normals_areas(rb, facesb);

normalsp = zeros(0, 3);
areasp   = zeros(0, 1);
[normalsp, areasp] = get_normals_areas(rp, facesp);


n       = [normalsb; normalsp];
areas   = [areasb;   areasp];

v = [rb; rp];

[G, dGn] = get_bem_matrices(v, n, areas); % dGn is dG(x, y)/dn(n)*area, A is G(x, y)*area


[curvb, normalsb] = get_surface_curvature(rb, normalsb);


[w] = get_tangential_velocity(facesb, rb, phib, areasb, normalsb);


nor_ar_p(:,1) = normalsp(:,1).*areasp;
nor_ar_p(:,2) = normalsp(:,2).*areasp;
nor_ar_p(:,3) = normalsp(:,3).*areasp;

%-------- получение Ip. can improve -----------------
%Ip = DTp*local_Ip*DTp';
Ip = zeros(3*NP, 3);
for i = 1:NP
    Ip((i-1)*3+1:i*3, :) = DTp((i-1)*3+1:i*3, :)*local_Ip((i-1)*3+1:i*3, :)*DTp((i-1)*3+1:i*3, :)';
end
%----------------------------------------------------

%-------- вычисление tau. can improve ---------------
tau = zeros(Nvert*NP, 3);
tmp_tau = cross(rp-kron(Rp, ones(Nvert, 1)), normalsp) ;
for i = 1:NP
    local_Ip
    Ip
    inv(Ip((i-1)*3+1:i*3, :))' 
    size( tau((i-1)*Nvert+1:i*Nvert, :) )
    size( inv(Ip((i-1)*3+1:i*3, :))' )
    size( tmp_tau((i-1)*Nvert+1:i*Nvert, :)' )
    tau((i-1)*Nvert+1:i*Nvert, :) = (rho_l*inv(Ip((i-1)*3+1:i*3, :))'*tmp_tau((i-1)*Nvert+1:i*Nvert, :)')';%возможно нужно транспонирование
end
%----------------------------------------------------


[qb, phip] = calcBEMsystem(G, dGn, normalsp, areasp, Up, phib, tau, Mp, rp);

%------- calc chi    --------------------------------
tmp_chi = cross(rp, normalsp, 2);
% reshape zabiil ti
chi = [tmp_chi(:,1).*phip.*areasp
       tmp_chi(:,2).*phip.*areasp
       tmp_chi(:,3).*phip.*areasp ];
chi = reshape(tmp_chi, Nvert, NP*3);
chi = sum(chi, 1);
chi = vec2mat(chi, NP)';
%----------------------------------------------------
%------- calc Lp    ---------------------------------
Lp = rho_l*(Mp+chi);
%----------------------------------------------------
%------- calc Omega_p    ----------------------------
Omega_p = zeros(NP, 3);
for i = 1:NP
    Omega_p(i, :) = inv(Ip((i-1)*3+1:i*3, :))*Lp(i,:)';
end
%----------------------------------------------------

%------- calc W_p    --------------------------------
Wp = zeros(3*NP, 3);
for i = 1:NP
    % Omega_p(i,1) -- Omega_px
    % Omega_p(i,2) -- Omega_py
    % Omega_p(i,3) -- Omega_pz
tmp_Wp = [0               -Omega_p(i,3)    Omega_p(i,2);
          Omega_p(i,3)    0                -Omega_p(i,1);
          -Omega_p(i,2)   Omega_p(i,1)     0]; 
Wp((i-1)*3+1:i*3, :) = tmp_Wp;
end
%----------------------------------------------------
for i = 1:NP
    dDTp = zeros(3*NP, 3);
    dDTp((i-1)*3+1:i*3, :) = Wp((i-1)*3+1:i*3, :)*DTp((i-1)*3+1:i*3, :);
end

[wp] = get_tangential_velocity(facesp, rp, phip, areasp, normalsp);


drb(:,1) = qb.*normalsb(:,1)+(1+alpha)*w(:,1);
drb(:,2) = qb.*normalsb(:,2)+(1+alpha)*w(:,2);
drb(:,3) = qb.*normalsb(:,3)+(1+alpha)*w(:,3);

nabla_phi_square = dot(drb',drb')';
ww = dot(w', drb')';

Pa = -Ap*sin(omega*time);
Pinf = P0 + Pa;


% [Vg] = P3DBL_get_volume_all(rb);
[Vg] = get_volume(rb, normalsb, areasb);
%part_phip =  kron(((Vg0./Vg).^kappa), ones(Nvert, 1)); %   kron(Pg0'.*((Vg0./Vg).^kappa), ones(Nvert, 1));
dphib = (nabla_phi_square/2+alpha*ww + ...
        ( Pinf - ...
        kron(Pg0'.*((Vg0./Vg).^kappa), ones(Nvert, 1)) + ...
        2*sigma*curvb)/rho_l-rb(:,3)*g);
    
dRp = zeros(0, 3);
dUp = zeros(0, 3);

phip_n_ar = [nor_ar_p(:,1).*phip ...
             nor_ar_p(:,2).*phip ...
             nor_ar_p(:,1).*phip];
phip_n_ar = reshape(phip_n_ar, Nvert, NP*3);
phip_n_ar = sum(phip_n_ar, 1);
phip_n_ar = vec2mat(phip_n_ar, NP)';



dRp = Up + rho_l/m_p*phip_n_ar;



% normalsp_Up = sum( normalsp .* kron(Up, ones(Nvert,1)), 2 );% F(x) -- переделать под вращение
F = sum( normalsp .* kron(Up, ones(Nvert,1)), 2 ) + sum( tau .* kron(Mp, ones(Nvert,1)), 2 );
normalsp_psy = sum( normalsp .* kron(phip_n_ar, ones(Nvert, 1)), 2 );

sizeTau = size(tau)
sizeChi = size(chi)
%qp = rho_l/m_p * normalsp_psy + normalsp_Up;
qp = rho_l/m_p * normalsp_psy + dot(tau, kron(chi, ones(Nvert, 1)), 2 ) + F;

drp(:,1)=qp.*normalsp(:,1)+wp(:,1);
drp(:,2)=qp.*normalsp(:,2)+wp(:,2);
drp(:,3)=qp.*normalsp(:,3)+wp(:,3);


nabla_phip_n_S = repmat(dot(drp',drp'), 3, 1)' .* nor_ar_p;
nabla_phip_n_S = reshape(nabla_phip_n_S, Nvert,  NP*3);
nabla_phip_n_S = sum(nabla_phip_n_S);
nabla_phip_n_S = vec2mat(nabla_phip_n_S, NP)';

q_nabla_phip = repmat(dot(nor_ar_p',drp'), 3, 1)' .* drp;
q_nabla_phip = reshape(q_nabla_phip, Nvert,  NP*3);
q_nabla_phip = sum(q_nabla_phip);
q_nabla_phip = vec2mat(q_nabla_phip, NP)';

dUp = repmat( (1 - rho_l/rho_p)*[0 0 g], NP, 1) + ...
   rho_l/m_p * ( (1/2)*nabla_phip_n_S ...
   - q_nabla_phip ) ; 
size(dUp)
disp('Должно быть')
[NP 3]

VolumeOfParticle = [4/3*pi*a*b*c];
g
size(rp)
size(repmat(dot(nor_ar_p',drp'), 3, 1)' .* drp)
tmp_dMp = cross(rp,  repmat(dot(nor_ar_p',drp'), 3, 1)' .* drp, 2);
tmp_dMp = reshape(tmp_dMp, Nvert,  NP*3);
tmp_dMp = sum(tmp_dMp);
tmp_dMp = vec2mat(tmp_dMp, NP)';
dMp = cross((rho_p/rho_l)*VolumeOfParticle*Rp, kron(g, ones(3, 1)) ) + tmp_dMp;

for i = 1:NB
    k=(Nvert*(i-1)+1:Nvert*i)';
    dphib(k) = Filter*dphib(k);
    drb(k,1) = Filter*drb(k,1);
    drb(k,2) = Filter*drb(k,2);
    drb(k,3) = Filter*drb(k,3);
end;


%конец функции
end

