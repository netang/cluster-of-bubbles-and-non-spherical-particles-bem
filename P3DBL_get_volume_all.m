function [Vol] = P3DBL_get_volume_all(v)

global faces NB  Bc

faceBN = size(faces, 1)/2;

a=zeros(1,3);
b=zeros(1,3);
c=zeros(1,3);

Vol=zeros(NB,1);

for bub=1:NB
    lf=faceBN*(bub-1)+1;
    rf=faceBN*bub;
    for n=lf:rf 
        a(1,:)=v(faces(n,1),:)-Bc(bub,:); b(1,:)=v(faces(n,2),:)-Bc(bub,:); c(1,:)=v(faces(n,3),:)-Bc(bub,:);
        Vol(bub)=Vol(bub)+abs(dot(a,cross(b,c)))/6;
    end;
end;    



