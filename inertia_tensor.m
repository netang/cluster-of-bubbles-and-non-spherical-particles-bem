function [ I ] = inertia_tensor( N, a, b, c, m )

M = [1/5*m*(b^2+c^2)   0                 0;
     0                 1/5*m*(a^2+c^2)   0;
     0                 0                 1/5*m*(a^2+b^2)];
I = zeros(3*N, 3);

for i = 1:N
    I((i-1)*3+1:i*3, :) = M;
end
I

end

