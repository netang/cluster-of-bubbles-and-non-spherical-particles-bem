
mhandles = guihandles(handleGraphic);
set(mhandles.time, 'String', num2str(time));


rp = rp_local_vert + kron(Rp, ones( Nvert, 1) );%исправить повторяются действия


v = [rb; rp]; % ��������

trimesh(facesb, rb(:,1), rb(:,2), rb(:,3), 'Facecolor', 'blue', 'EdgeColor', 'black', 'LineWidth', 1, 'FaceAlpha', 0.2); 
hold on
trimesh(facesp, rp(:,1), rp(:,2), rp(:,3), 'Facecolor', 'red', 'EdgeColor', 'black', 'LineWidth', 1, 'FaceAlpha', 0.2);
hold off;
% trimesh(faces, v(:,1), v(:,2), v(:,3), 'Facecolor', 'red', 'EdgeColor', 'black', 'LineWidth', 1, 'FaceAlpha', 0.2); lighting phong;
view([0 90]);
title('Bubbles and particles interaction');
xlabel('x'); ylabel('y'); zlabel('z');
axis equal;

c = 7;
xmin = -c*r0; xmax = 2*c*r0;
ymin = -c*r0; ymax = c*r0;
zmin = -c*r0; zmax = c*r0;

xlim([xmin xmax]); 
ylim([ymin ymax]);
zlim([zmin zmax]);


% [Bc] = get_centers(v,areas);
% [Vg] = get_volume(v,normals,areas);
% [Bc]=P3DBL_get_centers_mean(v);
[Bc] = get_centers_mean(v);
%[Vg]=P3DBL_get_volume_all(v);

% [Bc] = get_centers(rb, areasb); % может можно удалить
Vg_mas(hh,:) = Vg./Vg0;
Rp_mas(hh,:) = Rp;
Up_mas(hh,:) = Up;
Bc_mas(hh,:,:) = Bc;
time_mas(hh) = time/tmax;
hh = hh + 1;

drawnow;