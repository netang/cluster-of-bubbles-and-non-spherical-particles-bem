function [normals, areas] = get_normals_areas(v, faces)
% global facesb;
global r0
% size(faces)
% size(v)

Nvert = length(v);
Nface = length(faces);
areas = zeros(Nvert,1);
normals = zeros(Nvert,3);
v1 = faces(:,1);
v2 = faces(:,2);
v3 = faces(:,3);
r1=v(v1,:); r2=v(v2,:); r3=v(v3,:);
vec1=r2-r1; vec2=r3-r1; %����
% vec1 = r2-r1; vec2 = r1 - r3; ��
vec=.5*cross(vec1,vec2); %����
ar=sqrt(dot(vec',vec'))';
% vec = cross(vec1, vec2); ��
% ar=0.5*sqrt(dot(vec',vec'))';
for j=1:Nface
    areas(v1(j)) = areas(v1(j)) + ar(j);
    areas(v2(j)) = areas(v2(j)) + ar(j);
    areas(v3(j)) = areas(v3(j)) + ar(j);
    normals(v1(j),:) = normals(v1(j),:) + vec(j,:);
    normals(v2(j),:) = normals(v2(j),:) + vec(j,:);
    normals(v3(j),:) = normals(v3(j),:) + vec(j,:);
end;

areas=areas/3;
dnor=sqrt(dot(normals',normals')');
normals(:,1)=normals(:,1)./dnor;
normals(:,2)=normals(:,2)./dnor;
normals(:,3)=normals(:,3)./dnor;


%     n=.5*cross(x2-x1,x3-x1);
%     normal=n/sqrt(dot(n,n));
%     normal
%     centers(i,:)=x;
%     normals(i,:)=normal;
%*sqrt(sqrt(dot(n,n)))

% xnor = zeros(2,3);
% figure;
% trisurf(faces, v(:,1), v(:,2), v(:,3), 'FaceColor','yellow', 'FaceAlpha', 0.5); axis equal;
% hold on;
% for i=1:size(v, 1)
%     xnor(1,:) = v(i, :);
%     xnor(2,:) = v(i, :) + normals(i, :);
%     plot3(xnor(:, 1),xnor(:, 2),xnor(:, 3));
% end
% save('get_normals_areas_BP_bem.mat');
% error('1')