% Notation: In this code we use small p for index i.e. Hp is H_p for variable from sheme, 
%  and we use big P for inner programm variable names i.e. NP is Number of
%  Particle
% getters we write with little "g", i.e., example: getNormals(...),
% getInitParameters(...), ...
% Programm method structure: Main Scripts
%                            Global Methods
%                            Methods for Global Methods
%                            little methods (getters)
%                            

clear all;
close all;

GlobalVariables;

% GUI;

SetSystemParameters;

Init;

EvolutionCycle;
