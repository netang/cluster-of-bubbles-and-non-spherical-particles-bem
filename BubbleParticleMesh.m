function [ facesb, verticesb, facesp, verticesp, markFaces, markVertices, Nvert] = ...
         BubbleParticleMesh( Bc, Pc, Rb, Rp, frec )
%  BubbleParticleMesh is function for generate Bubbles and Particles mesh
%   Bc is array of bubbles centers
%   Pc is array of particle centers
%   Rb is array of Radius i-th bubbles
%   Rp us array of Radius i-th particles
%   frec is frequency of mesh discretisation
%   Run Example: BubbleParticleMesh( [0 0 0; 2 0 0], [0 4 0; 2 4 0], [0.7 0.5], [0.6 0.5], 2 );

[f, v] = SphereMeshZ(frec,1);
Nvert = size(v, 1);

verticesb = [];
facesb = [];
% markFaces = [];
% markVertices = [];

for i=1:size(Bc, 1)
    facesb = [facesb; f + size(verticesb, 1)];
    verticesb = [verticesb; [Rb(i)*v(:,1) + Bc(i,1) ... % увеличиваем радиус пузырька до нужного размера и перемещаем узлы.
        Rb(i)*v(:,2) + Bc(i,2) Rb(i)*v(:,3) + Bc(i,3)] ]; % можно переделать под index-method
%     markVertices = [markVertices; i*ones(size(v, 1), 1) ];
%     markFaces    = [markFaces;    i*ones(size(f,    1), 1) ];
end

verticesp = zeros(0, 3);
facesp = [];
markFaces = [];
markVertices = [];
for i=1:size(Pc, 1)
    facesp = [facesp; f + size(verticesp, 1)];
    verticesp = [verticesp; [Rp(i)*v(:,1) + Pc(i,1) ... % увеличиваем радиус пузырька до нужного размера и перемещаем узлы.
        Rp(i)*v(:,2) + Pc(i,2)     Rp(i)*v(:,3) + Pc(i,3)] ]; % можно переделать под index-method
%     markVertices = [markVertices; i*ones(size(v, 1), 1) ];
%     markFaces    = [markFaces;    i*ones(size(f,    1), 1) ];
end

% figure;
% trimesh(facesp, verticesp(:,1), verticesp(:,2), verticesp(:,3));

end

