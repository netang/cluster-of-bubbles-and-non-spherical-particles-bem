% This script consider global variables

global rp_local_vert NV NBV NPV NB NP Nvert
% NB is number of bubbles
% NP is number of particles
% NV is number of all vertices
% % NBV is number of bubble vertices
% % NPV is number of particle vertices

% Particle global variable
global m_p rho_p local_Ip
% m_p particle mass
% rho_p density of particle
% local_Ip constant inertia tensor

% Meh global variable
global faces facesb facesp
% faces is array of all faces
% facesb is array of bubbles faces
% facesp is array of particles

% Liquid global variable
global sigma rho_l mu;

% time global variable
global ht tmax hh;

% bubble global variable
global kappa

global AllNeighbors AllNeighborFaces NeiBookmarks

% programm's flags
global odemem odenomem

global Vg_mas time_mas Filter Rp_mas Up_mas Bc_mas

global pg0 P0 phog0 Vg0  mg0 omega Ap g r0 Pg0

global Bc Vg  alpha S coef_ab_n Rg T0

% Mesh global variable
global frec

global a b c % возможно потом придётся удалить

