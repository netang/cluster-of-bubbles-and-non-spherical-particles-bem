function [Bc] = get_centers_mean(v)

global NB Nvert
for ib=1:NB
    vb = zeros(Nvert,3);
    vb = v( (ib-1)*Nvert+1:ib*Nvert,:);
    Bc(ib,1)=mean(vb(:,1));
    Bc(ib,2)=mean(vb(:,2));
    Bc(ib,3)=mean(vb(:,3));
end;
