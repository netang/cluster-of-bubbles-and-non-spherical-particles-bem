function [vertices] = EllipsoidMesh(vertices,a,b,c)
% Builde Ellipsoid Mesh from sphere mesh

dist=sqrt(dot(vertices',vertices'))';
vertices(:,1)=a*vertices(:,1)./dist;
vertices(:,2)=b*vertices(:,2)./dist;
vertices(:,3)=c*vertices(:,3)./dist;

end