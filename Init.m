frec = 2; % частота дискретизации сетки.
[fF, vF] = SphereMeshZ(frec, 1); % vF нужна для создани фильтра

Bc = [ 5*r0       0*r0         0]; % Bubble centers for initial time
     % -2.5*r0      1.443*r0    0;
     %  2.5*r0      1.443*r0    0];
     % 0     -3*r0  0];
ParticleCenters = [0*r0    0*r0   0]; % zeros(0, 3); % Particle centers for initial time
                  % 5*r0     0*r0  0];
BubbleRadius   = [r0]; % радиусы пузырьков, соответственно.
ParticleRadius = [r0]; % радиусы частиц, соответственно.

[facesb, verticesb, facesp, verticesp, markFaces, markVertices, Nvert] = ...
    BubbleParticleMesh( Bc, ParticleCenters, BubbleRadius, ParticleRadius, frec );
faces = [facesb; facesp + size(verticesb, 1) ];


%---------------------------------------
% Из sphere mesh получаем Ellipsoid mesh
verticesp = EllipsoidMesh(verticesp, 2*r0, r0, r0);
% trimesh(facesp, verticesp(:,1), verticesp(:,2), verticesp(:,3), 'Facecolor', 'red', 'EdgeColor', 'black', 'LineWidth', 1, 'FaceAlpha', 0.2);
% hold on;
% trimesh(facesb, verticesb(:,1), verticesb(:,2), verticesb(:,3), 'Facecolor', 'blue', 'EdgeColor', 'black', 'LineWidth', 1, 'FaceAlpha', 0.2);
% hold off;
% view([0 90]);
% title('Bubbles and particles interaction');
% xlabel('x'); ylabel('y'); zlabel('z');
% axis equal;
% c = 7;
% xmin = -c*r0; xmax = 2*c*r0;
% ymin = -c*r0; ymax = c*r0;
% zmin = -c*r0; zmax = c*r0;
% 
% xlim([xmin xmax]); 
% ylim([ymin ymax]);
% zlim([zmin zmax]);
% 
%  error('1')
%---------------------------------------
a = 2*r0; b = r0; c = r0;
local_Ip = inertia_tensor( size(ParticleCenters, 1), a, b, c, 4/3*pi*a*b*c*rho_p );

m_p = 4/3*pi*a*b*c*rho_p;

%
% verticesp = zeros(0, 3);
% [facesb, verticesb, Bc0,  Nvert, faceBN0, indv0] = TwoBubbleGenerator_reflection(frec,r0);
% faces = facesb;
%

% All vertices = [verticesb; verticesp]

%Nvert - количество вершин в сетках (одинаковое для всех пока)
NB = size(Bc, 1); %number of bubbles
NP = size(ParticleCenters, 1); %number of particles
NBV = NB * Nvert;
NPV = NP * Nvert;

% будет работать, если количество вершин в сетках частиц одинаковое, да и
% вообще они все будут одинаковыми, если радиусы частичек будут совпадать
rp_local_vert = verticesp - kron(ParticleCenters, ones( Nvert, 1) );

%----------------------------------------------------------------
rb = verticesb;
phib = zeros(length(rb), 1); % potential value on bubbles nodes
Rp = ParticleCenters;
Up = zeros(NP, 3); %[0 0 0; 0 0 0];
DTp = zeros(3*NP, 3);
for i = 1:NP
    DTp((i-1)*3+1:i*3, :) = eye(3, 3);
end
Mp = zeros(NP, 3);
%-----------------------------------------------------------------




%timing
tmin = 0;
tmax = tmax0;
ht = ht0; 
ntf = fout;
nts = fsave; % oh =(
nt = round( (tmax-tmin)/ht )+1; tmax = ht*(nt-1); 
ts = ht*linspace(0,nt-1,nt);
touts = ts(1:ntf:nt); nout = length(touts)+1;
touts(nout)=touts(nout-1)+ntf*ht;
tsaves=ts(1:nts:nt); nsave=length(tsaves)+1;
tsaves(nsave) = tsaves(nsave-1)+nts*ht;
hh = 1;
ht = ht0;
tmax = tmax0; % oh =(


% mesh constant parameters
[AllNeighbors, AllNeighborFaces, NeiBookmarks] = ...
    SetVertexNeighborFaceDataStructure( faces );

[Pg0, phog0, mg0] = get_init_parameter( rb, BubbleRadius );


[normalsb, areasb] = get_normals_areas(rb, facesb);
[Vg0] = get_volume(rb, normalsb, areasb);
[Filter] = matrix_filter(vF);
[coef_ab_n] = get_coef_ab_n(odemem,type_scheme);




[Bc] = get_centers(rb, areasb); % здесь  нужно?
[Vg] = get_volume(rb, normalsb, areasb); % нужно?



