
% ================  PHYSICAL PROPERTIES OF SYSTEM  ====================
mu = 0;             % liquid dynamic viscosity
rho_l = 1e+3;       % liquid density
sigma = 0.073;      % surface tension
g = 9.8;            % gravity constant
rho_p = 2000;

P0 = 1e+5;              % initial liquid pressure  
T0 = 293;
Rg = 8.31;              % gas constant
omega = 200*pi*2e+3;    % frequency of acoustic field 
Ap = 0.5*P0;%0.5*P0;            % amplitude of acoustic field
kappa = 1.4;            % polytropic exponent
alpha = 0;%-1;             % parameter of tangential velocity corrector

%time integration
periodNumb = 5;
tmax0 = periodNumb*2*pi/omega;     % time period;   
ht0 = 1e-8; %1e-8;%1e-9;             % integration timestep
fout = 1;              %output frequency (output happens after every frout timesteps)

r0 = 1e-5; % начальный радиус пузырьков и частиц.
% ====================================================================



% ================  PROGRAM FLAGS  ====================
%emergency backup frequency
fsave = 1000;          %backup data necessary for continuation every fsave integration timesteps
%type of solvers used
type_scheme = 2; % 1 - AB scheme, 2 - ABM scheme
multistep_mem = 1;%6;%4;     %length of prehistory in the solver (should be consisten with the type of the solver (e.g. 4 means that 3 previous rhs will be saved, which is consistent with AB4 solver)
odenomem = 4;          %solver without prehistory
odemem = 6;            %solver with prehistory 

%iterative process
iterative = 0;        % 0 = no iterative process; 1=use iterative process;
maxnit = 10;          % max number of iterations allowed per time step
toliter = 1e-6;       % tolerance for termination of the iterative process
%======================================================
