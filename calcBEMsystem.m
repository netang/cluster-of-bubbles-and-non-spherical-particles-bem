function [ qb, phip ] = calcBEMsystem( G, M, normalsp, areasp, Up, phib, tau, DEsystemMp, rp )
%CALCBEMSYSTEM return qb and phip for RHS
% M is dG(x,y)/dn(x)*S(x)

%------ global variables ----------------
global NBV NB NPV
% NB - number of bubbles
% NP - number of particles
% NBV - number of vertices all bubbles
% NPV - number of vetiices all particles
global Nvert
%Nvert - number of vertices in one mesh
global rho_l m_p
%----------------------------------------



Np = G(:,NB*Nvert+1:end)*normalsp;
Kp = G(:,NB*Nvert+1:end)*tau; % проверить
% Hp = sum( Np*Up', 2 ); 
Hp = sum( Np*Up', 2 ) + sum( Kp*DEsystemMp', 2 ); % проверить
% Qp = rho_l/m_p * Np*(normalsp .* repmat( areasp, 1, 3))';
Qp = rho_l/m_p * Np*(normalsp .* repmat( areasp, 1, 3))' + Kp*(cross(rp, normalsp) .* repmat( areasp, 1, 3))' ; % дописать

Mp = M(:, NBV+1:end);
Jp = Mp - Qp;


Mb = M(:, 1:NBV);


% MIH = (Mb - [1/2*eye(NBV, NBV); 1/2*eye(NPV, NBV)])*phib ...
MIH = (Mb - [1/2*eye(NBV, NBV); zeros(NPV, NBV)])*phib ...
    - Hp; % Variable for M_b + 1/2*I*[phi_b] - H_p matrices for bubble and particle


Lb = G(:, 1:NBV);
LJ = [Lb  -Jp];


qb_phip = LJ\MIH;
qb = qb_phip(1:NBV);
phip = qb_phip(NBV+1:end);

end

